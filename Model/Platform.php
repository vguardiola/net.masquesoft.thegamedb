<?php

namespace Mqs\theGameDBBundle\Model;

/**
 * Class Platform
 *
 * @package Mqs\theGameDBBundle\Model
 */
class Platform
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $platform;
    /**
     * @var string
     */
    private $console;
    /**
     * @var string
     */
    private $controller;
    /**
     * @var string
     */
    private $overview;
    /**
     * @var string
     */
    private $developer;
    /**
     * @var string
     */
    private $manufacturer;
    /**
     * @var string
     */
    private $cpu;
    /**
     * @var string
     */
    private $memory;
    /**
     * @var string
     */
    private $graphics;
    /**
     * @var string
     */
    private $sound;
    /**
     * @var string
     */
    private $display;
    /**
     * @var string
     */
    private $media;
    /**
     * @var integer
     */
    private $maxcontrollers;
    /**
     * @var float
     */
    private $rating;
    /**
     * @var \ArrayObject
     */
    private $images;
    /**
     * @var string
     */
    private $baseImageUrl;


    public function __construct()
    {
        $this->images = new \ArrayObject();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = (integer) $id;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = (string) $platform;
    }

    /**
     * @return string
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * @param string $console
     */
    public function setConsole($console)
    {
        $this->console = (string) $console;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController($controller)
    {
        $this->controller = (string) $controller;
    }

    /**
     * @return string
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * @param string $overview
     */
    public function setOverview($overview)
    {
        $this->overview = (string) $overview;
    }

    /**
     * @return string
     */
    public function getDeveloper()
    {
        return $this->developer;
    }

    /**
     * @param string $developer
     */
    public function setDeveloper($developer)
    {
        $this->developer = (string) $developer;
    }

    /**
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param string $manufacturer
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = (string) $manufacturer;
    }

    /**
     * @return string
     */
    public function getCpu()
    {
        return $this->cpu;
    }

    /**
     * @param string $cpu
     */
    public function setCpu($cpu)
    {
        $this->cpu = (string) $cpu;
    }

    /**
     * @return string
     */
    public function getMemory()
    {
        return $this->memory;
    }

    /**
     * @param string $memory
     */
    public function setMemory($memory)
    {
        $this->memory = (string) $memory;
    }

    /**
     * @return string
     */
    public function getGraphics()
    {
        return $this->graphics;
    }

    /**
     * @param string $graphics
     */
    public function setGraphics($graphics)
    {
        $this->graphics = (string) $graphics;
    }

    /**
     * @return string
     */
    public function getSound()
    {
        return $this->sound;
    }

    /**
     * @param string $sound
     */
    public function setSound($sound)
    {
        $this->sound = (string) $sound;
    }

    /**
     * @return string
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param string $display
     */
    public function setDisplay($display)
    {
        $this->display = (string) $display;
    }

    /**
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param string $media
     */
    public function setMedia($media)
    {
        $this->media = (string) $media;
    }

    /**
     * @return integer
     */
    public function getMaxcontrollers()
    {
        return $this->maxcontrollers;
    }

    /**
     * @param integer $maxcontrollers
     */
    public function setMaxcontrollers($maxcontrollers)
    {
        $this->maxcontrollers = (integer) $maxcontrollers;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     */
    public function setRating($rating)
    {
        $this->rating = (float) $rating;
    }

    /**
     * @return \ArrayObject
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param \ArrayObject $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @param mixed $image
     */
    public function addImages($image)
    {
        $this->images->append($image);
    }


    /**
     * @param $baseImgUrl
     */
    public function setBaseImageUrl($baseImgUrl)
    {
        $this->baseImageUrl = $baseImgUrl;
    }

    /**
     * @return mixed
     */
    public function getBaseImageUrl()
    {
        return $this->baseImageUrl;
    }
}