<?php

namespace Mqs\theGameDBBundle\Model;

class Similar
{
    private $id;
    private $platformId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * @param mixed $platformId
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = $platformId;
    }
}