<?php

namespace Mqs\theGameDBBundle\Model;

/**
 * Class Game
 *
 * @package Mqs\theGameDBBundle\Model
 */
class Game
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $gameTitle;
    /**
     * @var string
     */
    private $platform;
    /**
     * @var integer
     */
    private $platformId;
    /**
     * @var \DateTime
     */
    private $releaseDate;
    /**
     * @var string
     */
    private $overview;
    /**
     * @var string
     */
    private $ESRB;
    /**
     * @var string
     */
    private $genres;
    /**
     * @var string
     */
    private $youtube;
    /**
     * @var string
     */
    private $publisher;
    /**
     * @var string
     */
    private $developer;
    /**
     * @var float
     */
    private $rating;
    /**
     * @var \ArrayObject
     */
    private $similar;
    /**
     * @var \ArrayObject
     */
    private $images;

    /**
     * @var string
     */
    private $coop;
    /**
     * @var string
     */
    private $baseImageUrl;

    /**
     * @var string
     */
    private $players;

    /**
     * Game constructor.
     */
    public function __construct()
    {
        $this->similar = new \ArrayObject();
        $this->images = new \ArrayObject();
    }

    /**
     * @return string
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param string $players
     */
    public function setPlayers($players)
    {
        $this->players = (string) $players;
    }

    /**
     * @return mixed
     */
    public function getCoop()
    {
        return $this->coop;
    }

    /**
     * @param mixed $coop
     */
    public function setCoop($coop)
    {
        $this->coop = (string) $coop;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return mixed
     */
    public function getGameTitle()
    {
        return $this->gameTitle;
    }

    /**
     * @param mixed $gameTitle
     */
    public function setGameTitle($gameTitle)
    {
        $this->gameTitle = (string) $gameTitle;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = (string) $platform;
    }

    /**
     * @return \DateTime
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * @param string $releaseDate
     */
    public function setReleaseDate($releaseDate)
    {
        $this->releaseDate = new \DateTime($releaseDate);
    }

    /**
     * @return string
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * @param string $overview
     */
    public function setOverview($overview)
    {
        $this->overview = (string) $overview;
    }

    /**
     * @return string
     */
    public function getESRB()
    {
        return $this->ESRB;
    }

    /**
     * @param string $ESRB
     */
    public function setESRB($ESRB)
    {
        $this->ESRB = (string) $ESRB;
    }

    /**
     * @return mixed
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @param mixed $genres
     */
    public function setGenres($genres)
    {
        $this->genres = (array) $genres;
    }

    /**
     * @return mixed
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param mixed $youtube
     */
    public function setYoutube($youtube)
    {
        $this->youtube = (string) $youtube;
    }

    /**
     * @return mixed
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * @param mixed $publisher
     */
    public function setPublisher($publisher)
    {
        $this->publisher = (string) $publisher;
    }

    /**
     * @return mixed
     */
    public function getDeveloper()
    {
        return $this->developer;
    }

    /**
     * @param mixed $developer
     */
    public function setDeveloper($developer)
    {
        $this->developer = (string) $developer;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = (float) $rating;
    }

    /**
     * @return \ArrayObject
     */
    public function getSimilar()
    {
        return $this->similar;
    }

    /**
     * @param \ArrayObject $similar
     */
    public function setSimilar($similar)
    {
        $this->similar = (array) $similar;
    }

    /**
     * @param $similar
     */
    public function addSimilar($similar)
    {
        $this->similar->append($similar);
    }

    /**
     * @return \ArrayObject
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param \ArrayObject $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @param $art
     */
    public function addImages($art)
    {
        $this->images->append($art);
    }

    /**
     * @return string
     */
    public function getBaseImageUrl()
    {
        return $this->baseImageUrl;
    }

    /**
     * @param string $baseImgUrl
     */
    public function setBaseImageUrl($baseImgUrl)
    {
        $this->baseImageUrl = (string) $baseImgUrl;
    }

    /**
     * @return integer
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * @param integer $platformId
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = (int) $platformId;
    }


}