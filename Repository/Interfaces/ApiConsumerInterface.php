<?php

namespace Mqs\theGameDBBundle\Repository\Interfaces;

interface ApiConsumerInterface
{
    /**
     * @param array $params name: Performs a search for a game by title - Alpha characters only, case insensitive,
     *     partial phrases accepted, no partial words. exactname: Performs a search for a game by exact title - Case
     *     insensitive, exact title matches only.id: ID representing a specific game. platform: Platform to filter
     *     results by, Alpha-Numeric characters only, See this wiki article for a list of valid platforms
     *     API_ValidPlatformsList
     *
     * @return mixed
     */
    public function getGame($params);

    /**
     * @param string      $name
     * @param string|null $platform filters results by platform
     * @param string|null $genre filters results by genre
     *
     * @return mixed
     */
    public function getGameList($name, $platform = null, $genre = null);

    /**
     * @param integer $id The numeric ID of the game in our database that you like to fetch artwork details for.
     *
     * @return mixed
     */
    public function getArt($id);

    /**
     * @param integer $id The numeric ID of the platform in our database that you like to fetch metadata and artwork
     *     data for.
     *
     * @return mixed
     */
    public function getPlatform($id);

    /**
     * @return mixed
     */
    public function getPlatformList();

    /**
     * @param integer $platformId the integer id of the required platform, as retrived from GetPlatformsList)
     *
     * @return mixed
     */
    public function getPlatformGames($platformId);

    /**
     * @param integer $time time in seconds
     *
     * @return mixed
     */
    public function getUpdates($time);
}