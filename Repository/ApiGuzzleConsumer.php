<?php

namespace Mqs\theGameDBBundle\Repository;

use GuzzleHttp\Client;
use Mqs\theGameDBBundle\Model\Art;
use Mqs\theGameDBBundle\Model\Game;
use Mqs\theGameDBBundle\Model\Platform;
use Mqs\theGameDBBundle\Model\Similar;
use Mqs\theGameDBBundle\Repository\Interfaces\ApiConsumerInterface;

class ApiGuzzleConsumer implements ApiConsumerInterface
{

    private $baseUrl = 'http://thegamesdb.net/api/';
    /** @var  Client */
    private $httpClient;

    public function __construct($httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $params
     *
     * @return mixed|void
     */
    public function getGame($params)
    {
        $url = $this->baseUrl.'GetGame.php?'.http_build_query($params);
        $xml = $this->getXmlResponse($url);
        $game = new Game();
        $game->setId($xml->Game->id);
        $game->setGameTitle($xml->Game->GameTitle);
        $game->setPlatform($xml->Game->Platform);
        $game->setPlatformId($xml->Game->PlatformId);
        $game->setReleaseDate($xml->Game->ReleaseDate);
        $game->setOverview($xml->Game->Overview);
        $game->setESRB($xml->Game->ESRB);
        $game->setGenres($xml->Game->Genres);
        $game->setYoutube($xml->Game->youtube);
        $game->setPublisher($xml->Game->Publisher);
        $game->setDeveloper($xml->Game->Developer);
        $game->setCoop($xml->Game->{'Co-op'});
        $game->setRating($xml->Game->Rating);
        $game->setBaseImageUrl($xml->baseImgUrl);
        $game->setPlayers($xml->Players);
        foreach ($xml->Game->Similar->Game as $value) {
            $similar = new Similar();
            $similar->setId($value->id);
            $similar->setPlatformId($value->PlatformId);
            $game->addSimilar($similar);
        }
        $this->extractFanart($xml->Game->Images->fanart, $game, 'fanart');
        $this->extractFanart($xml->Game->Images->screenshot, $game, 'screenshot');
        $this->extractArt($xml->Game->Images->boxart, $game, 'boxart');
        $this->extractArt($xml->Game->Images->clearlogo, $game, 'clearlogo');

        return $game;
    }

    /**
     * @param $url
     *
     * @return mixed
     */
    private function getXmlResponse($url)
    {
        try {
            $xml = simplexml_load_string($this->httpClient->get($url)->getBody()->getContents());
        } catch (\Exception $e) {
            $xml = new \SimpleXMLElement('');
        }

        return $xml;
    }

    /**
     * @param               $fanart
     * @param Platform|Game $platform
     * @param string        $type
     */
    private function extractFanart($fanart, $platform, $type)
    {
        foreach ($fanart as $value) {
            $art = new Art();
            $art->setOriginal($platform->getBaseImageUrl().$value->original);
            $art->setThumb($value->thumb ? $platform->getBaseImageUrl().$value->thumb : null);
            $art->setWidth($value->original['width']);
            $art->setHeight($value->original['height']);
            $art->setType($type);
            $platform->addImages($art);
        }
    }

    /**
     * @param               $art
     * @param Platform|Game $platform
     * @param string        $type
     */
    private function extractArt($art, $platform, $type)
    {
        foreach ($art as $value) {
            $art = new Art();
            $art->setOriginal($platform->getBaseImageUrl().$value);
            $art->setThumb($value['thumb'] ? $platform->getBaseImageUrl().$value['thumb'] : null);
            $art->setWidth($value['width']);
            $art->setHeight($value['height']);
            $art->setSide($value['side']);
            $art->setType($type);
            $platform->addImages($art);
        }
    }

    public function getGameList($name, $platform = null, $genre = null)
    {
        $url = $this->baseUrl.'GetGamesList.php?name='.$name;
        $url = (null !== $platform) ? $url.'&platform='.$platform : $url;
        $url = (null !== $genre) ? $url.'&genre='.$genre : $url;
        $xml = $this->getXmlResponse($url);
        $response = [];
        foreach ($xml->Game as $item) {
            $response[] = (array) $item;
        }

        return $response;
    }

    public function getArt($id)
    {
        $url = $this->baseUrl.'GetArt.php?id='.$id;
        $xml = $this->getXmlResponse($url);
        $game = new Game();
        $game->setBaseImageUrl($xml->baseImgUrl);
        $this->extractFanart($xml->Images->fanart, $game, 'fanart');
        $this->extractFanart($xml->Images->screenshot, $game, 'screenshot');
        $this->extractArt($xml->Images->boxart, $game, 'boxart');
        $this->extractArt($xml->Images->clearlogo, $game, 'clearlogo');

        return $game->getImages();
    }

    public function getPlatformList()
    {
        $url = $this->baseUrl.'GetPlatformsList.php';
        $xml = $this->getXmlResponse($url);
        $response = [];
        foreach ($xml->Platforms->Platform as $item) {
            $response[] = (array) $item;
        }

        return $response;
    }

    public function getPlatform($id)
    {
        $url = $this->baseUrl.'GetPlatform.php?id='.$id;
        $xml = $this->getXmlResponse($url);
        $platform = new Platform();
        $platform->setId($xml->Platform->id);
        $platform->setController(
            str_replace(
                'http://www.youtube.com/watch?v=',
                'http://thegamesdb.net/banners/platform/controllerart/',
                $xml->Platform->controller
            )
        );
        $platform->setConsole(
            str_replace(
                'http://www.youtube.com/watch?v=',
                'http://thegamesdb.net/banners/platform/controllerart/',
                $xml->Platform->console
            )
        );
        $platform->setRating($xml->Platform->Rating);
        $platform->setCpu($xml->Platform->cpu);
        $platform->setMedia($xml->Platform->media);
        $platform->setMemory($xml->Platform->memory);
        $platform->setDeveloper($xml->Platform->developer);
        $platform->setManufacturer($xml->Platform->manufacturer);
        $platform->setGraphics($xml->Platform->graphics);
        $platform->setSound($xml->Platform->sound);
        $platform->setDisplay($xml->Platform->display);
        $platform->setMaxcontrollers($xml->Platform->maxcontrollers);
        $platform->setPlatform($xml->Platform->Platform);
        $platform->setOverview($xml->Platform->overview);
        $platform->setBaseImageUrl($xml->baseImgUrl);
        $this->extractFanart($xml->Platform->Images->fanart, $platform, 'fanart');
        $this->extractArt($xml->Platform->Images->boxart, $platform, 'boxart');
        $this->extractArt($xml->Platform->Images->banner, $platform, 'banner');

        return $platform;
    }

    public function getPlatformGames($platformId)
    {
        $url = $this->baseUrl.'GetPlatformGames.php?platform='.$platformId;
        $xml = $this->getXmlResponse($url);
        $response = [];
        foreach ($xml->Game as $item) {
            $response[] = (array) $item;
        }

        return $response;
    }

    /**
     * @param integer $time time in seconds
     *
     * @return mixed
     */
    public function getUpdates($time)
    {
        $url = $this->baseUrl.'Updates.php?time='.$time;
        $xml = $this->getXmlResponse($url);
        $response = [];
        foreach ($xml->Game as $item) {
            $response[] = (int) $item;
        }

        return $response;
    }
}