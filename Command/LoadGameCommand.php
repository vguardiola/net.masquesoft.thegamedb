<?php
namespace Mqs\theGameDBBundle\Command;

use GuzzleHttp\Client;
use Mqs\theGameDBBundle\Repository\ApiGuzzleConsumer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadGameCommand extends Command
{
    protected function configure()
    {
        $this->setName('thegamedb:load:game')
            ->setDescription('Load data from theGameDb')
            ->addOption('id', null, InputOption::VALUE_REQUIRED)
            ->setHelp(
                <<<EOT
<info>php %command.full_name% [--redirect-uri=...] [--grant-type=...] name</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api = new ApiGuzzleConsumer(new Client());
        dump(
            $api->getGame(
                [
                    'id' => $input->getOption('id'),
                ]
            )
        );
    }
}
