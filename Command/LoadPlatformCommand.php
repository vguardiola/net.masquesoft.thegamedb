<?php
namespace Mqs\theGameDBBundle\Command;

use GuzzleHttp\Client;
use Mqs\theGameDBBundle\Repository\ApiGuzzleConsumer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadPlatformCommand extends Command
{
    protected function configure()
    {
        $this->setName('thegamedb:load:platform')
            ->setDescription('Load data from theGameDb')
            ->addOption('id', null, InputOption::VALUE_OPTIONAL)
            ->setHelp(
                <<<EOT
<info>php %command.full_name% [--redirect-uri=...] [--grant-type=...] name</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api = new ApiGuzzleConsumer(new Client());
        if (null !== $input->getOption('id')) {
            $paltform = $api->getPlatform($input->getOption('id'));
        } else {
            $paltform = $api->getPlatformList();
        }
        dump($paltform);
    }
}
