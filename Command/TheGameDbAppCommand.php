<?php
namespace Mqs\theGameDBBundle\Command;

use GuzzleHttp\Client;
use Mqs\theGameDBBundle\Model\Game;
use Mqs\theGameDBBundle\Model\Platform;
use Mqs\theGameDBBundle\Repository\ApiGuzzleConsumer;
use Mqs\theGameDBBundle\Repository\Interfaces\ApiConsumerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class TheGameDbAppCommand extends Command
{
    protected function configure()
    {
        $this->setName('thegamedb:app')
            ->setDescription('Load data from theGameDb')
            ->setHelp(
                <<<EOT
<info>php %command.full_name% [--redirect-uri=...] [--grant-type=...] name</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api = new ApiGuzzleConsumer(new Client());
        $dialog = new QuestionHelper();
        $question = new Question(
            [
                "<comment>1</comment>: View <info>G</info>ame\n",
                "<comment>2</comment>: <info>S</info>earch game\n",
                "<comment>3</comment>: View <info>P</info>latform\n",
                "<comment>4</comment>: Get Games on P<info>l</info>atform\n",
                "<comment>x</comment>: E<info>x</info>it\n",
                "<question>Choose a type:</question> ",
            ]
        );
        do {
            $option = $dialog->ask($input, $output, $question);
            switch (strtolower(trim($option))) {
                case 1:
                case 'g':
                    $gameId = $dialog->ask($input, $output, new Question('<question>Game ID?</question> '));
                    if ((int) $gameId > 0) {
                        $game = $api->getGame(['id' => $gameId]);
                        $this->drawGameProfile($game, $output);
                        $dialog->ask($input, $output, new Question('<question>Any key to exit</question>'));
                    }
                    break;
                case 2:
                case 's':
                    $gameName = $dialog->ask($input, $output, new Question('<question>Search by name:</question> '));
                    if (null !== $gameName) {
                        dump($api->getGame(['name' => $gameName]));
                        $dialog->ask($input, $output, new Question('<question>Any key to exit</question>'));
                    }
                    break;
                case 3:
                case 'p':
                    $this->getPlatform($input, $output, $api, $dialog);
                    break;
                case 4:
                case 'l':
                    $this->getPlatformGameList($input, $output, $api, $dialog);
                    break;
            }
        } while ($option !== 'x');
    }

    /**
     * @param Game            $game
     * @param OutputInterface $output
     *
     * @internal param Game $id
     */
    private function drawGameProfile(Game $game, OutputInterface $output)
    {
        $tableLeft = new Table($output);
        $tableLeft->setHeaders([new TableCell($game->getGameTitle(), ['colspan' => 3])]);
        $tableLeft->addRow(
            ['Platform', $game->getPlatform(), new TableCell(wordwrap($game->getOverview(), 40), ['rowspan' => 5])]
        );
        $tableLeft->addRow(['Release Date', $game->getReleaseDate()->format('Y-m-d')]);
        $tableLeft->addRow(['Players', $game->getPlayers()]);
        $tableLeft->addRow(['Co-op', $game->getCoop()]);
        $tableLeft->addRow(['Developer', $game->getDeveloper()]);
        $tableLeft->addRow(['Publisher', $game->getPublisher()]);
        $tableLeft->addRow(['Generes', implode(',', $game->getGenres())]);
        $tableLeft->render();
    }

    /**
     * @param InputInterface       $input
     * @param OutputInterface      $output
     * @param ApiConsumerInterface $api
     * @param QuestionHelper       $dialog
     *
     * @return mixed
     */
    protected function getPlatform(InputInterface $input, OutputInterface $output, $api, $dialog)
    {
        $platforms = $api->getPlatformList();
        $this->drawPlatformList($platforms, $output);
        $platformId = $dialog->ask(
            $input,
            $output,
            new Question('<question>View platform # ?</question> [<comment>#|E<info>x</info>it</comment>]')
        );
        if (preg_match('!\d+!i', $platformId)) {
            $platform = $api->getPlatform($platformId);
            $this->drawPlatormProfile($platform, $output);
            $this->goPreviousOrMainMenu($input, $output, $api, $dialog, 'getPlatform');
        }
    }

    /**
     * @param mixed           $platforms
     * @param OutputInterface $output
     *
     * @return mixed
     *
     */
    protected function drawPlatformList($platforms, OutputInterface $output)
    {
        $table = new Table($output);
        $table->setHeaders(['id', 'Name', 'id', 'Name', 'id', 'Name']);
        $row = [];
        foreach ($platforms as $platform) {
            $row[] = $platform['id'];
            $row[] = $platform['name'];
            if (count($row) === 6) {
                $table->addRow($row);
                $row = [];
            }
        }
        $table->render();

    }

    /**
     * @param Platform        $platform
     * @param OutputInterface $output
     *
     * @internal param Game $id
     */
    private function drawPlatormProfile(Platform $platform, OutputInterface $output)
    {
        $tableLeft = new Table($output);
        $tableLeft->setHeaders([new TableCell($platform->getPlatform(), ['colspan' => 3])]);
        $tableLeft->addRow(
            [
                'Developer',
                $platform->getDeveloper(),
                new TableCell(wordwrap($platform->getOverview(), 40), ['rowspan' => 6]),
            ]
        );
        $tableLeft->addRow(['Manufacturer', $platform->getManufacturer()]);
        $tableLeft->addRow(['CPU', $platform->getCpu()]);
        $tableLeft->addRow(['Memory', $platform->getMemory()]);
        $tableLeft->addRow(['Max. Controllers', $platform->getMaxcontrollers()]);
        $tableLeft->addRow(['Media', $platform->getMedia()]);
        $tableLeft->addRow(['Graphics', $platform->getGraphics()]);
        $tableLeft->addRow(['Sound', wordwrap($platform->getSound(), 40)]);
        $tableLeft->addRow(['Display', $platform->getDisplay()]);
        $tableLeft->render();
    }

    /**
     * @param InputInterface       $input
     * @param OutputInterface      $output
     * @param ApiConsumerInterface $api
     * @param QuestionHelper       $dialog
     * @param string               $previous
     */
    protected function goPreviousOrMainMenu(InputInterface $input, OutputInterface $output, $api, $dialog, $previous)
    {
        $action = $dialog->ask(
            $input,
            $output,
            new Question('<question>Prev | Main Menu</question> [<comment>p|m</comment>]')
        );
        if (strtolower($action) === 'p') {
            $this->$previous($input, $output, $api, $dialog);
        } else {
            return;
        }
    }

    /**
     * @param InputInterface       $input
     * @param OutputInterface      $output
     * @param ApiConsumerInterface $api
     * @param QuestionHelper       $dialog
     *
     * @return mixed
     */
    protected function getPlatformGameList(InputInterface $input, OutputInterface $output, $api, $dialog)
    {
        $platforms = $api->getPlatformList();
        $this->drawPlatformList($platforms, $output);
        $platformId = $dialog->ask(
            $input,
            $output,
            new Question('<question>View platform # ?</question> [<comment>#|E<info>x</info>it</comment>]')
        );
        if (preg_match('!\d+!i', $platformId)) {
            $items = $api->getPlatformGames($platformId);
            $this->drawGameList($items, $output);
            $this->goPreviousOrMainMenu($input, $output, $api, $dialog, 'getPlatformGameList');
        }
    }

    /**
     * @param mixed           $items
     * @param OutputInterface $output
     *
     * @return mixed
     */
    protected function drawGameList($items, OutputInterface $output)
    {
        $table = new Table($output);
        $table->setHeaders([new TableCell('Plataform', ['colspan' => 6])]);
        $table->setHeaders(['id', 'Game Title', 'id', 'Game Title', 'id', 'Game Title']);
        $row = [];
        foreach ($items as $item) {
            $row[] = $item['id'];
            $row[] = $item['GameTitle'];
            if (count($row) === 6) {
                $table->addRow($row);
                $row = [];
            }
        }
        $table->render();
    }
}
